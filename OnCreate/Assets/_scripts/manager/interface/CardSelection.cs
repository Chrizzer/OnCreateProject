﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CardSelection : MonoBehaviour {

    public GameObject cardInfoParent;
    public GameObject allCardsParent;
    public GameObject selectedCardsParent; // aka Deck

    public GameObject startGame;

    public Vector2 cardSize;
    public int cardsInARow;
    public Card[] allCards;
    public Card[] selectedCards;

    public int col, row;

    void Start ()
    {
        RectTransform rt = allCardsParent.GetComponent<RectTransform>();
        // Interface Update
        float cellSize = Mathf.Max(rt.rect.width / col, rt.rect.height / col);
        allCardsParent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellSize, cellSize);

        // Render selectedCards
        Vector2 size = InterfaceManager.Instance.ScreenSize;

        cardSize = new Vector2(cardSize.x * size.x, cardSize.y * size.x);

        Vector2 selectedCardsSize = new Vector2(size.x, cardSize.y);
        Vector2 infoSize = new Vector2(size.x * 0.3f, size.y - cardSize.y);
        Vector2 infoPos = new Vector2(0f, cardSize.y);
        Vector2 allCardsSize = new Vector2(size.x * 0.7f, size.y - cardSize.y);
        Vector2 allCardsPos = new Vector2(size.x * 0.3f, cardSize.y);

        allCardsSize = allCardsParent.GetComponent<RectTransform>().sizeDelta;
        // selectedCardsParent.GetComponent<RectTransform>().sizeDelta = selectedCardsSize;
        // allCardsParent.GetComponent<RectTransform>().sizeDelta = allCardsSize;
        // allCardsParent.GetComponent<RectTransform>().position = allCardsPos;

        // cardInfoParent.GetComponent<RectTransform>().sizeDelta = infoSize;
        // cardInfoParent.GetComponent<RectTransform>().position = infoPos;

        int len = selectedCards.Length;
        float xPos = size.x * 0.125f;
        Vector2 pos = new Vector2(xPos, 0f);
        /*
        for (int i = 0; i < len; i++)
        {
            GameObject cardGO = InterfaceManager.Instance.CreateCardGUI(selectedCards[i], cardSize, pos * i);
            cardGO.transform.SetParent(selectedCardsParent.transform);
        }
        */
        float yPos = size.y - cardSize.y;
        len = allCards.Length;
        for (int i = 0; i < len; i++)
        {
            //      i < 8  -> i / 8 = 0
            //  8 < i < 16 -> i / 8 = 1
            // 16 < i < 24 -> i / 8 = 2
            pos = new Vector2(infoSize.x*1.1f + xPos * (i % cardsInARow), yPos - (cardSize.y * (i / cardsInARow)));
            GameObject cardGO = InterfaceManager.Instance.CreateCardGUI(allCards[i], cardSize, pos, AddCardToDeck);
            cardGO.transform.SetParent(allCardsParent.transform);
        }
        cardInfoParent.GetComponent<CardInfo>().UpdatedDisplay(allCards[0]);
        RectTransform startGameRT = startGame.GetComponent<RectTransform>();
        startGameRT.sizeDelta = allCardsSize;
        startGameRT.position = allCardsPos;
        startGame.SetActive(false);
    }
    public GameObject mainMenu;
    public void BackToMainMenu()
    {
        gameObject.SetActive(false);
        mainMenu.SetActive(true);
    }
    public bool IsCardInDeck(Card card)
    {
        return selectedCards.Contains(card);
    }
    private int DeckSize(int n=0, int acc=0)
    {
        if (n < selectedCards.Length)
        {
            if (selectedCards[n] == null)
            {
                return DeckSize(n + 1, acc);
            }
            else
            {
                return DeckSize(n + 1, ++acc);
            }
        }
        else
        {
            return acc;
        }
    }
    private bool inProgressOfReplacing;
    public void AddCardToDeck(Card card)
    {
        bool isCardInDeck = IsCardInDeck(card);
        int len = selectedCards.Length;
        int deckSize = DeckSize();
        // Case 1 : Card is already in the deck
        if (isCardInDeck)
        {
            // Do Nothing
            Debug.Log(card.name + " is already in deck");
        }
        // Case 2 : Card is not in the deck and replacing is in progress
        else if (inProgressOfReplacing)
        {
            ReplaceCardInDeck(card);
        }
        // Case 2: Card is not in the deck and the deck is not full
        else if (deckSize < len)
        {
            // Vector stuff
            Vector2 size = InterfaceManager.Instance.ScreenSize;
            float xPos = size.x * 0.125f;
            Vector2 pos = new Vector2(xPos, 0f);

            selectedCards[deckSize] = card;
            // GameObject cardGO = InterfaceManager.Instance.CreateCardGUI(card, cardSize, pos * deckSize, ReplaceCardInDeck);
            GameObject cardGO = InterfaceManager.Instance.CreateCardGUI(card, cardSize, pos * deckSize, (x) => x.ToString());

            // Add the card to your last position
            cardGO.transform.SetParent(selectedCardsParent.transform);
            cardGO.transform.SetSiblingIndex(deckSize);
        }
        else
        {
            // Deck is full
            Debug.Log("Something weird happened " + card.name + " - " + deckSize);
        }
        if (DeckSize() == selectedCards.Length)
        {
            // Start the game because why not
            PersistentDataManager.Instance.player_deck = selectedCards;
            allCardsParent.SetActive(false);
            startGame.SetActive(true);
        }
    }
    public void RemoveCard(Card card)
    {
        int index = Array.IndexOf(selectedCards, card);
        selectedCards[index] = null;
    }
    private Card cardToReplace;
    public void ReplaceCardInDeck(Card card)
    {
        if (inProgressOfReplacing)
        {
            int indexOfCardToReplace = Array.IndexOf(selectedCards, cardToReplace);
            if (IsCardInDeck(card))
            {
                int index = Array.IndexOf(selectedCards, card);
                // Replace cards in deck
                Debug.Log("Deck[" + cardToReplace.name + "(" + indexOfCardToReplace + ") <-> " + card.name + "(" + index + ")]");

                selectedCards[indexOfCardToReplace] = card;
                selectedCards[index] = cardToReplace;

                GameObject cardThatGetsReplacedGO = selectedCardsParent.transform.GetChild(indexOfCardToReplace).gameObject;
                GameObject cardToReplaceGO = selectedCardsParent.transform.GetChild(index).gameObject;

                Image image_cardThatsGetsReplacedGO = cardThatGetsReplacedGO.transform.GetChild(2).GetComponent<Image>();
                Image image_cardToReplaceGO = cardToReplaceGO.transform.GetChild(2).GetComponent<Image>();

                image_cardThatsGetsReplacedGO.sprite = card.cardSprite;
                image_cardToReplaceGO.sprite = cardToReplace.cardSprite;

                cardThatGetsReplacedGO.transform.SetSiblingIndex(index);
                cardToReplaceGO.transform.SetSiblingIndex(indexOfCardToReplace);
            }
            else
            {
                // Replace card from deck with collection
                Debug.Log("Collection[" + cardToReplace.name + "(" + indexOfCardToReplace + ") <-> " + card.name+"]");

                selectedCards[indexOfCardToReplace] = card;
                GameObject bufferedCard = selectedCardsParent.transform.GetChild(indexOfCardToReplace).gameObject;

                Image imageBufferedCard = bufferedCard.transform.GetChild(2).GetComponent<Image>();

                imageBufferedCard.sprite = card.cardSprite;
            }
        }
        else
        {
            cardToReplace = card;
        }
        inProgressOfReplacing = !inProgressOfReplacing;
    }

    public void PickCard(Card card, GameObject cardGO, Card otherCard, GameObject otherCardGO)
    {
        bool isCardInDeck = IsCardInDeck(card);
        bool isOtherCardInDeck = IsCardInDeck(otherCard);
        int childIndexCard = cardGO.transform.GetSiblingIndex();
        int childIndexOtherCard = otherCardGO.transform.GetSiblingIndex();
        if (isCardInDeck && isOtherCardInDeck)
        {
            // Both cards are already in the selectedCards
            int indexOfCard = Array.IndexOf(selectedCards, card);
            int indexOfOthercard = Array.IndexOf(selectedCards, otherCard);

            selectedCards[indexOfOthercard] = card;
            selectedCards[indexOfCard] = otherCard;

            cardGO.transform.SetSiblingIndex(childIndexOtherCard);
            otherCardGO.transform.SetSiblingIndex(childIndexCard);
        }
        else if(isOtherCardInDeck)
        {
            int indexOfCardToReplace = Array.IndexOf(selectedCards, otherCard);
            selectedCards[indexOfCardToReplace] = card;

            cardGO.transform.SetParent(selectedCardsParent.transform);
            Destroy(otherCardGO);
            // otherCardGO.transform.SetParent(allCardsParent.transform);

            cardGO.transform.SetSiblingIndex(childIndexOtherCard);
        }
        else if(isCardInDeck)
        {
            int indexOfCardToReplace = Array.IndexOf(selectedCards, card);
            selectedCards[indexOfCardToReplace] = otherCard;

            Destroy(cardGO);
            // cardGO.transform.SetParent(allCardsParent.transform);
            otherCardGO.transform.SetParent(selectedCardsParent.transform);

            otherCardGO.transform.SetSiblingIndex(childIndexCard);
        }
    }
}
