﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardInfo : MonoBehaviour {

    public Image sprite;
    // [DEPRECATED]
    public Text cardname;
    public GameObject indicatorPrefab;
    private Text cost;
    private Text healthpoints;
    private Text attackpoints;
    private Text movementSpeed;
    private Text attackrange;
    private Text attackspeed;

    // Interface Update
    public GameObject i1, i2, i3, i4, i5, i6, iD, iN;
    private TextMeshProUGUI t1, t2, t3, t4, t5, t6, tD, tN;
    void Awake()
    {
        t1 = i1.GetComponentInChildren<TextMeshProUGUI>();
        t2 = i2.GetComponentInChildren<TextMeshProUGUI>();
        t3 = i3.GetComponentInChildren<TextMeshProUGUI>();
        t4 = i4.GetComponentInChildren<TextMeshProUGUI>();
        t5 = i5.GetComponentInChildren<TextMeshProUGUI>();
        t6 = i6.GetComponentInChildren<TextMeshProUGUI>();

        tD = iD.GetComponentInChildren<TextMeshProUGUI>();
        tN = iN.GetComponentInChildren<TextMeshProUGUI>();
        i3.SetActive(true);
        i4.SetActive(true);
        i5.SetActive(true);
        i6.SetActive(true);
    }
    void Start2()
    {
        GameObject costGO = Instantiate(indicatorPrefab);
        costGO.name = "Cost Indicator";
        GameObject hpGO = Instantiate(indicatorPrefab);
        hpGO.name = "Healthpoint Indicator";
        GameObject apGO = Instantiate(indicatorPrefab);
        apGO.name = "Attackpoints Indicator";
        GameObject msGO = Instantiate(indicatorPrefab);
        msGO.name = "Movementspeed Indicator";
        GameObject arGO = Instantiate(indicatorPrefab);
        arGO.name = "Attackrange Indicator";
        GameObject asGO = Instantiate(indicatorPrefab);
        asGO.name = "Attackspeed Indicator";

        cost = costGO.GetComponentInChildren<Text>();
        healthpoints = hpGO.GetComponentInChildren<Text>();
        attackpoints = apGO.GetComponentInChildren<Text>();
        movementSpeed = msGO.GetComponentInChildren<Text>();
        attackrange = arGO.GetComponentInChildren<Text>();
        attackspeed = asGO.GetComponentInChildren<Text>();

        cost.text = "Cost";
        healthpoints.text = "Healthpoints";
        attackpoints.text = "Attackpoints";
        movementSpeed.text = "MovementSpeed";
        attackrange.text = "Attackrange";
        attackspeed.text = "Attackspeed";

        costGO.transform.SetParent(transform);
        hpGO.transform.SetParent(transform);
        apGO.transform.SetParent(transform);
        msGO.transform.SetParent(transform);
        arGO.transform.SetParent(transform);
        asGO.transform.SetParent(transform);

        Vector2 size = GetComponent<RectTransform>().sizeDelta;

        RectTransform spriteRT = sprite.GetComponent<RectTransform>();
        spriteRT.sizeDelta = new Vector2(size.x, size.y * 0.85f);
        spriteRT.position = new Vector2(0f, size.y * 0.3f);

        RectTransform nameRT = cardname.GetComponent<RectTransform>();
        nameRT.sizeDelta = new Vector2(size.x, size.y * 0.1f);
        nameRT.position = new Vector2(0f, spriteRT.position.y + spriteRT.sizeDelta.y);

        size = new Vector2(size.x, size.y * 0.2f);
        int inRow = 3;
        Vector2 indicatorSize = new Vector2((size.x / inRow), size.x * 0.1f);
        // float posX = size.x * 0.1f;
        // float posY = size.y * 0.1f;
        GameObject[] gos = new GameObject[] { costGO, hpGO, apGO, msGO, arGO, asGO };
        // Text[] texts = new Text[] { healthpoints, attackpoints, movementSpeed, attackrange, attackspeed };
        int len = gos.Length;
        for (int i = 0; i < len; i++)
        {
            RectTransform rt = gos[i].GetComponent<RectTransform>();
            rt.sizeDelta = indicatorSize * 0.9f;
            rt.localPosition = new Vector2(indicatorSize.x * 0.05f + (indicatorSize.x) * (i % inRow), (size.y - indicatorSize.y) * (i / inRow));
        }
    }
    void Update2()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButton(0) && gameObject.activeInHierarchy)
            {
                gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began && gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }

    public void UpdatedDisplay(Card card)
    {
        if (card is Unit)
        {
            Unit unit = card as Unit;
            i1.SetActive(true);
            i2.SetActive(true);
            //   HP | AD
            //   MS | AS
            // Cost | AR
            t1.text = unit.baseHealthPoints.ToString();
            t2.text = unit.baseAttackPoints.ToString();
            t3.text = unit.baseMovementSpeed.ToString();
            t4.text = unit.baseAttackSpeed.ToString();
            // Cost is always updated, see below
            t6.text = unit.baseAttackRange.ToString();
        }
        else if (card is Spell)
        {
            Spell spell = card as Spell;
            i1.SetActive(false);
            i2.SetActive(false);
            // Target | Radius
            //   Cost | Speed
            t3.text = spell.affectedLayers.Aggregate("", (x, acc) => x + "\n" + acc);
            t4.text = spell.baseRadius.ToString();
            t6.text = spell.baseSpeed.ToString();
        }
        sprite.sprite = card.defaultSprite;
        // Properties updated regardless of type
        tD.text = card.description;
        tN.text = card.name;
        t5.text = card.baseCost.ToString();
        gameObject.SetActive(true);
    }
    public void Display(Card card)
    {
        if (card is Unit)
        {
            healthpoints.gameObject.SetActive(true);
            attackpoints.gameObject.SetActive(true);
            movementSpeed.gameObject.SetActive(true);
            attackrange.gameObject.SetActive(true);
            attackspeed.gameObject.SetActive(true);
            Display(card as Unit);
        }
        else if (card is Spell)
        {
            healthpoints.gameObject.SetActive(false);
            attackpoints.gameObject.SetActive(false);
            movementSpeed.gameObject.SetActive(false);
            attackrange.gameObject.SetActive(false);
            attackspeed.gameObject.SetActive(false);
            Display(card as Spell);
        }
    }
    public void Display(Unit card)
    {
        /* 
        Values to display 
            Name     
            Cost
            Healthpoints
            Attackpoints
            MovementSpeed
            AttackRange
            AttackSpeed
            Sprite
        */
        cardname.text = card.name;
        cost.text = "Cost " + card.baseCost.ToString();
        healthpoints.text = "HP " + card.baseHealthPoints.ToString();
        attackpoints.text = "AP " + card.baseAttackPoints.ToString();
        movementSpeed.text = "MS " + Unit.Translate(card.baseMovementSpeed).ToString();
        attackrange.text = "AR " + card.baseAttackRange.ToString();
        attackspeed.text = "AS " + card.baseAttackSpeed.ToString();
        sprite.sprite = card.defaultSprite;
    }
    public void Display(Spell card)
    {
        cardname.text = card.name;
        cost.text = "Cost " + card.baseCost.ToString();
        sprite.sprite = card.defaultSprite;
    }
}
