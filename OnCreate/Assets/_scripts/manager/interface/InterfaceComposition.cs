﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(menuName = "OnCreate/Interface/Composition")]
public class InterfaceComposition : ScriptableObject
{
    public new string name;
    public InterfaceElement[] elements;
    private GameObject gameObject;

    public void Inflate(InterfaceManager manager, Vector2 screenSize)
    {
        if (gameObject == null)
        {
            gameObject = new GameObject(name);
            gameObject.transform.SetParent(manager.canvas.gameObject.transform);
        }
        // Vector2 v = elements.Select(x => x.size).Aggregate((x, y) => x + y);
        Vector2 v = Vector2.one * 100f;
        int len = elements.Length;
        for (int i = 0; i < len; i++)
        {
            float sizeX = (elements[i].size.x / v.x) * screenSize.x;
            float sizeY = (elements[i].size.y / v.y) * screenSize.y;
            float posX = (elements[i].position.x / v.x) * screenSize.x;
            float posY = (elements[i].position.y / v.y) * screenSize.y;

            Vector2 size = new Vector2(sizeX, sizeY);
            Vector2 position = new Vector2(posX, posY);

            elements[i].Build(gameObject, size, position);
        }
    }
}
