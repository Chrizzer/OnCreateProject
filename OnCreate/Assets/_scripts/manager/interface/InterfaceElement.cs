﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class InterfaceElement
{
    public string name;
    private GameObject gameObject;
    public Vector2 size;
    public Vector2 position;

    public void Build(GameObject parent, Vector2 elementSize, Vector2 elementPosition)
    {
        if (gameObject == null)
        {
            gameObject = new GameObject(name);
        }
        gameObject.transform.SetParent(parent.transform);
        gameObject.Ext_AddOrGetCompononent<CanvasRenderer>();
        Image im = gameObject.Ext_AddOrGetCompononent<Image>();
        im.rectTransform.sizeDelta = elementSize;
        Vector2 translatedPosition = (elementSize / 2f) + elementPosition;
        Align(im, translatedPosition);
    }
    private void Align(Image im, Vector2 elementPosition)
    {
        im.rectTransform.pivot = Vector2.one / 2f;
        im.rectTransform.anchorMin = Vector2.one / 2f;
        im.rectTransform.anchorMax = Vector2.one / 2f;

        im.rectTransform.anchoredPosition = elementPosition;
    }
}
