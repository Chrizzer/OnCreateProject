﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spell : Card {

    public enum Target
    {
        Friendly, Enemy
    }

    [Header("Spell Properties")]
    [Range(1f, 50f)]
    public float baseRadius;
    [Range(1f, 50f)]
    public float baseSpeed;
    public GameObject prefab;
    public Target[] affectedLayers;

    public abstract void ApplySpell(SpellTarget target);
}