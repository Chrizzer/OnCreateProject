﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Cards/Spell/New Damage Spell")]
public class DamageSpell : Spell
{
    [Range(0, 1000)]
    public int baseDamage;

    public override void ApplySpell(SpellTarget target)
    {
        target.health.TakeDmg(baseDamage);
    }
}
