﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [CreateAssetMenu(menuName = "OnCreate/Cards/Spell/New Buff Spell")]
public abstract class BuffSpell : Spell
{
    // If duration is infinite ( until death ) -1
    [Range(-1f, 60f)]
    public float duration;
    public UnitPropertyType[] affectedTypes;
    // Some sort of effect
    // Some sort of visualization
}
