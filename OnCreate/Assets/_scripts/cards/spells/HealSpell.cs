﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Cards/Spell/New Heal Spell")]
public class HealSpell : Spell
{
    [Range(0, 1000)]
    public int baseHeal;

    public override void ApplySpell(SpellTarget target)
    {
        target.health.Heal(baseHeal);
    }
}
