﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "OnCreate/Cards/Unit")]
public class Unit : Card
{
    public enum MovementSpeed
    {
        Very_Slow, Slow, Medium, Fast, Very_Fast
    }

    [Header("Unit Properties")]
    [Range(0,1000)]
    public int baseHealthPoints;
    [Range(0,1000)]
    public int baseAttackPoints;
    public MovementSpeed baseMovementSpeed;
    [Range(1, 10)]
    public float baseAttackRange;
    [Range(1, 3)]
    public float baseAttackSpeed;
    public Healthbar healthbar;
    public GameObject prefab;
    /// <summary>
    /// Translates the MovementSpeed into an int for various purposes.
    /// </summary>
    /// <param name="speed"></param>
    /// <returns></returns>
    public static int Translate(MovementSpeed speed)
    {
        switch (speed)
        {
            case MovementSpeed.Very_Slow:
                return 1;
            case MovementSpeed.Slow:
                return 2;
            case MovementSpeed.Medium:
                return 3;
            case MovementSpeed.Fast:
                return 4;
            case MovementSpeed.Very_Fast:
                return 5;
            default:
                return 0;
        }
    }
}