﻿using System.Collections;
using UnityEngine;

public class UnitController : MonoBehaviour, IReuseable {

    public bool debug;
    public Unit unit;
    public int attackPoints;
    public float attackRange;
    public float attackSpeed;
    public LayerMask enemyMask;
    public Unit.MovementSpeed movementSpeed;
    public Transform target;
    private HealthbarController health;
    private Rigidbody2D rb;
    public PlayerController commander;
    private int myIndex;
    // Update to new Spellsystem starts here
    public SpellTarget spellTarget;

    private void OnDisable()
    {
        commander.Release(this, myIndex);
    }

    public void Ready(PlayerController commander, int myIndex)
    {
        gameObject.transform.position = commander.SpawnPosition;
        if (this.commander == null)
        {
            this.myIndex = myIndex;
            this.commander = commander;
            SetupGO();
            Setup();
        }
        else
        {
            Reset();
        }
        StartCoroutine(Routine());
    }

    void Setup(
        int startingAttackPoints = -1,
        float startingAttackRange = -1f,
        float startingAttackSpeed = -1f,
        int startingHP = -1)
    {
        this.attackPoints = CheckInt(startingAttackPoints, unit.baseAttackPoints);
        this.attackRange = CheckFloat(startingAttackRange, unit.baseAttackRange);
        this.attackSpeed = CheckFloat(startingAttackSpeed, unit.baseAttackSpeed);
        this.movementSpeed = unit.baseMovementSpeed;
    }

    private void Reset(
        int startingAttackPoints = -1,
        float startingAttackRange = -1f,
        float startingAttackSpeed = -1f,
        int startingHP = -1)
    {
        gameObject.SetActive(true);
        health.Reset(unit.baseHealthPoints, startingHP);
        this.attackPoints = CheckInt(startingAttackPoints, unit.baseAttackPoints);
        this.attackRange = CheckFloat(startingAttackRange, unit.baseAttackRange);
        this.attackSpeed = CheckFloat(startingAttackSpeed, unit.baseAttackSpeed);
        this.movementSpeed = unit.baseMovementSpeed;
    }

    void SetupGO()
    {
        health = gameObject.Ext_AddOrGetCompononent<HealthbarController>();
        health.healthbar = unit.healthbar;
        rb = gameObject.Ext_AddOrGetCompononent<Rigidbody2D>();
        rb.freezeRotation = true;
        rb.isKinematic = true;
        // To be redone once we figure out animation stuff I guess
        SpriteRenderer unitSprite = GetComponentInChildren<SpriteRenderer>();
        if (unitSprite != null)
        {
            GameObject child = unitSprite.gameObject;
            CapsuleCollider2D collider = child.Ext_AddOrGetCompononent<CapsuleCollider2D>();
            unitSprite.sprite = unit.defaultSprite;
            child.transform.localScale = unit.spriteScale;
            collider.size = unit.colliderSize;
            collider.offset = unit.colliderOffset;
            child.transform.SetParent(transform);
            child.transform.localPosition = Vector3.zero;
            child.layer = gameObject.layer;
        }
        else
        {
            GameObject child = new GameObject("Unit", typeof(SpriteRenderer));
            unitSprite = child.GetComponent<SpriteRenderer>();
            CapsuleCollider2D collider = child.Ext_AddOrGetCompononent<CapsuleCollider2D>();
            unitSprite.sprite = unit.defaultSprite;
            child.transform.localScale = unit.spriteScale;
            collider.size = unit.colliderSize;
            collider.offset = unit.colliderOffset;
            child.transform.SetParent(transform);
            child.transform.localPosition = Vector3.zero;
            child.layer = gameObject.layer;
        }
        // until here ^
        health.Setup(unit);
        // Set this unit up to be targetable by spells
        spellTarget = gameObject.Ext_AddOrGetCompononent<SpellTarget>();
    }
    private int CheckInt(int toCheck, int against)
    {
        if (toCheck < 0){ return against; }
        else{ return toCheck; }
    }
    private float CheckFloat(float toCheck, float against)
    {
        if (toCheck < 0f) { return against; }
        else { return toCheck; }
    }
    private IEnumerator Routine()
    {
        HealthbarController enemyHC = null;
        int enemyLayer = enemyMask;
        HealthbarController goal = target.gameObject.GetComponent<HealthbarController>();
        Vector3 bounds = transform.FindChild("Unit").GetComponent<SpriteRenderer>().bounds.size;
        float attackOffset = bounds.x / 2f;
        // While the unit is alive
        while (health.IsAlive)
        {
            // Choose next enemy
            Vector3 toTarget = target.transform.position - transform.position;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, toTarget, attackOffset + attackRange, enemyLayer);
            
            // If there is no enemy in range
            if (debug) Debug.DrawRay(transform.position, toTarget.normalized * (attackOffset+attackRange), Color.red, Time.deltaTime);
            if (hit.collider != null)
            {
                enemyHC = hit.collider.gameObject.transform.parent.GetComponent<HealthbarController>() as HealthbarController;
                if (debug) Debug.Log(name + " FOUND " + enemyHC.name);
            }
            if (enemyHC == null)
            {
                // Move to the next enemy
                Vector3 step = toTarget.normalized * Time.deltaTime * Unit.Translate(movementSpeed);
                transform.position += step;
                yield return null;
            }
            else
            {
                // Attack the next enemy
                Attack(enemyHC);
                if (debug) Debug.Log(name + " ATTACKS " + enemyHC.name);
                if (!enemyHC.IsAlive)
                {
                    enemyHC = null;
                }
                yield return new WaitForSeconds(attackSpeed);
            }
            if (!goal.IsAlive)
            {
                break;
            }
        }
    }
    public void Attack(HealthbarController other)
    {
        other.TakeDmg(attackPoints);
    }
}
