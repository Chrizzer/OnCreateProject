﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellController : MonoBehaviour, IReuseable
{
    public Spell spell;
    public BuildingController building;
    public float spellSpeed;
    private bool flinging;
    private bool abort;

    private PlayerController commander;
    private int myIndex;

    public void Abort()
    {
        abort = true;
    }
    private void OnDisable()
    {
        commander.Release(this, myIndex);
    }

    public void Ready(PlayerController commander, int myIndex)
    {
        gameObject.transform.position = commander.SpellPosition;
        Physics2D.IgnoreCollision(commander.allyBase.GetComponentInChildren<BoxCollider2D>(), GetComponent<CircleCollider2D>());
        this.spellSpeed = spell.baseSpeed;
        this.flinging = false;
        if (this.commander == null)
        {
            this.myIndex = myIndex;
            this.commander = commander;
        }
        gameObject.SetActive(true);
        if (Application.isEditor)
        {
            StartCoroutine(ListeningEditor());
        }
        else
        {
            StartCoroutine(ListeningAndroid());
        }
    }

    private IEnumerator ListeningEditor()
    {
        while (!abort)
        {
            if (Input.GetMouseButton(0) && !flinging)
            {
                Vector3 pos = Input.mousePosition;
                pos.z = 0f;
                FlingSpell(Camera.main.ScreenToWorldPoint(pos));
            }
            yield return null;
        }
    }
    private IEnumerator ListeningAndroid()
    {
        while (!abort)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began && !flinging)
                {
                    Vector3 pos = Input.GetTouch(i).position;
                    pos.z = 0f;
                    FlingSpell(Camera.main.ScreenToWorldPoint(pos));
                }
            }
            yield return null;
        }
    }
    private void FlingSpell(Vector3 destination)
    {
        transform.position = building.transform.position;
        StartCoroutine(Flying(destination));
    }
    private IEnumerator Flying(Vector3 destination)
    {
        flinging = true;
        Vector3 start = building.gameObject.transform.position;
        Vector3 step = (destination - transform.position).normalized;
        // While spell is not there
        while (Vector3.Distance(transform.position, destination) > (spell.baseRadius / 2f))
        {
            transform.position += step * spellSpeed * Time.deltaTime;
            yield return null;
        }
        flinging = false;
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (flinging)
        {
            SpellTarget target = collision.gameObject.transform.parent.GetComponent<SpellTarget>() as SpellTarget;
            if (target != null)
            {
                bool isTarget = DirtyOr(target.gameObject.layer);
                if (isTarget)
                {
                    spell.ApplySpell(target);
                }
                else
                {
                    Debug.Log(target.name + " is no target of " + spell.name);
                }
            }
        }
    }

    private bool DirtyOr(LayerMask mask)
    {
        bool acc = false;
        int len = spell.affectedLayers.Length;
        LayerMask friendlyLayer = commander.gameObject.layer;
        LayerMask enemyLayer = commander.gameObject.layer;
        for (int i = 0; i < len; i++)
        {
            switch (spell.affectedLayers[i])
            {
                case Spell.Target.Friendly:
                    acc = acc || mask == friendlyLayer;
                    break;
                case Spell.Target.Enemy:
                    acc = acc || mask == enemyLayer;
                    break;
                default:
                    Debug.Log("Forgot to add a layer in SpellController DirtyAnd");
                    break;
            }
        }
        return acc;
    }
}