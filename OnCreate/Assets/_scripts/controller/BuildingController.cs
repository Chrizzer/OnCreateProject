﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour {

    public Building building;
    private HealthbarController healthbar;

    void Start ()
    {
        healthbar = GetComponent<HealthbarController>();
        healthbar.Setup(building);
        // StartCoroutine(WaitTillDie());
    }
    private IEnumerator WaitTillDie()
    {
        Func<bool> f = () => !healthbar.IsAlive;
        yield return new WaitUntil(f);
        Debug.Log(name + " just died");
    }
    private void OnCollisionEnter2D3(Collision2D collision)
    {
        Debug.LogFormat("Building {0} got hit by {1}",building.name, collision.gameObject.name);
    }
}