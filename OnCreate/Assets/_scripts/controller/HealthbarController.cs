﻿using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;

public class HealthbarController : MonoBehaviour {

    public bool debug;
    public Healthbar healthbar;
    public int baseHealthPoints;
    public int healthPoints;
    public bool IsAlive
    {
        get { return this.healthPoints > 0; }
    }
    private GameObject healthbarGO;
    private SpriteRenderer healthbarRenderer;
    private SpriteRenderer sprite;
    public void Reset(int baseHP, int startingHP = -1)
    {
        healthPoints = (startingHP < 0) ? baseHP : startingHP;
        baseHealthPoints = baseHP;
        UpdateHealthbarColor();
        StartCoroutine(VisualizeHPChange());
    }

    public void Setup(int baseHP, int startingHP = -1)
    {
        healthPoints = (startingHP < 0) ? baseHP : startingHP;
        baseHealthPoints = baseHP;
        SetupHealthbarGO();
        StartCoroutine(VisualizeHPChange());
    }
    public void Setup(Building b, int startingHP = -1)
    {
        Setup(b.baseHealthPoints, startingHP);
    }
    public void Setup(Unit u, int startingHP = -1)
    {
        Setup(u.baseHealthPoints, startingHP);
    }
    
    /* TextMeshPro broke Coroutines for whatever reason
    TextMeshPro indicator;
    private void SetUpIndicator(Vector3 position, Vector3 size)
    {
        GameObject tmp = new GameObject("Indicator");
        tmp.transform.SetParent(transform);
        indicator = tmp.AddComponent<TextMeshPro>();
        indicator.text = "";
        indicator.color = Color.white;
        indicator.alignment = TextAlignmentOptions.Center;
        indicator.fontSize = 16;
        indicator.sortingOrder = 1;
        indicator.overflowMode = TextOverflowModes.Overflow;
        indicator.enableWordWrapping = false;
        RectTransform rt = indicator.GetComponent<RectTransform>();
        rt.sizeDelta = size;
        float xOffset = Random.Range(-size.x / 6f, size.x / 6f);
        float yOffset = size.y + size.y * 1.25f;
        rt.localPosition = new Vector3(position.x + xOffset, position.y - yOffset, position.z);
        // StartCoroutine(VisualizeHPChange());
    }
    private IEnumerator VisualizeHPChange(int amount)
    {
        indicator.alpha = 255f;
        // If lastHP were higher than current HP you took dmg
        indicator.color = (amount < 0) ? Color.red : Color.green;
        indicator.text = Positive(amount).ToString();
        yield return new WaitForSeconds(0.5f);
        indicator.alpha = 0f;
    }
    private IEnumerator VisualizeHPChange2()
    {
        Debug.Log("Coroutine VisualizeHPChange startet für " + name);
        // float timePassed = 0f;
        indicator.alpha = 0f;
        indicator.text = string.Empty;
        // int increment = Mathf.CeilToInt(damage / duration);
        int lastHP = healthPoints;
        while (healthPoints > 0 && gameObject.activeInHierarchy)
        {
            bool hpChanged = lastHP != healthPoints;
            if (hpChanged)
            {
                indicator.alpha = 255f;
                // If lastHP were higher than current HP you took dmg
                indicator.color = (lastHP > healthPoints) ? Color.red : Color.green;
                indicator.text = (Positive(lastHP - healthPoints)).ToString();
                lastHP = healthPoints;
                yield return new WaitForSeconds(0.5f);
                indicator.alpha = 0f;
            }
            // timePassed += Time.deltaTime;
            yield return null;
        }
        Debug.Log("Coroutine VisualizeHPChange endet für " + name);
    }
    */
    private IEnumerator VisualizeHPChange2()
    {
        float duration = 0.5f;
        float timePassed = 0f;
        if (sprite == null)
        {
            sprite = transform.GetComponentsInChildren<SpriteRenderer>().Where(x => x.name != "Healthbar").FirstOrDefault();
        }
        Color start = sprite.color;
        while (timePassed < duration)
        {
            timePassed += Time.deltaTime;
            // Quadratic function -> -4x² + 4x : Special thanks to magnificient Julian B.D. Tinat
            float x = timePassed / duration;
            float y = -4f * x * x + 4f * x;
            sprite.color = Color.Lerp(start, Color.red, y);
            yield return null;
        }
    }
    private IEnumerator VisualizeHPChange3(Color color)
    {
        float duration = 0.5f;
        float timePassed = 0f;
        if (sprite == null)
        {
            sprite = transform.GetComponentsInChildren<SpriteRenderer>().Where(x => x.name != "Healthbar").FirstOrDefault();
        }
        Color start = Color.white;
        while (timePassed < duration)
        {
            timePassed += Time.deltaTime;
            // Quadratic function -> -4x² + 4x : Special thanks to magnificient Julian B.D. Tinat
            float x = timePassed / duration;
            float y = -4f * x * x + 4f * x;
            sprite.color = Color.Lerp(start, color, y);
            yield return null;
        }
    }
    private IEnumerator VisualizeHPChange()
    {
        int lastHP = healthPoints;
        while (healthPoints > 0 && IsAlive)
        {
            bool hpChanged = lastHP != healthPoints;
            if (hpChanged)
            {
                int amount = lastHP - healthPoints;
                UpdateHealthbarColor();
                yield return VisualizeHPChange3((amount > 0) ? Color.red : Color.green);
                lastHP = healthPoints;
            }
            yield return null;
        }
    }
    public void Heal(int amount)
    {
        healthPoints += amount;
        // StartCoroutine(VisualizeHPChange());
        // UpdateHealthbarColor();
    }
    private int Positive(int amount)
    {
        return (amount < 0) ? -amount : amount;
    }
    public void TakeDmg(int damage)
    {
        healthPoints -= damage;
        // UpdateHealthbarColor();
        // StartCoroutine(VisualizeHPChange());
        if (healthPoints < 0)
        {
            Die();
        }
    }
    public void UpdateHealthbarColor()
    {
        healthbarRenderer.color = healthbar.GetColor(this.healthPoints, baseHealthPoints);
    }
    public delegate void OnDeath();
    public event OnDeath OnDeathHandler;
    public void SubscribeDeathFunction(OnDeath f)
    {
        OnDeathHandler += f;
    }
    public void UnsubscribeDeathFunction(OnDeath f)
    {
        OnDeathHandler -= f;
    }
    public void Die()
    {
        healthPoints = 0;
        if (debug) Debug.Log(name + " took fatal damage");
        // Deathrattle effects
        if (OnDeathHandler != null)
        {
            OnDeathHandler();
        }
        sprite.color = Color.white;
        StopAllCoroutines();
        gameObject.SetActive(false);
    }
    private void SetupHealthbarGO()
    {
        if (healthbarGO == null)
        {
            // Check for an existing SpriteRenderer
            Transform t = transform.FindChild("Healthbar");
            if (t == null)
            {
                healthbarGO = new GameObject("Healthbar", typeof(SpriteRenderer));
                healthbarGO.transform.SetParent(transform);
            }
            else
            {
                healthbarGO = t.gameObject;
            }
            // Setup the SpriteRenderer
            healthbarRenderer = healthbarGO.GetComponent<SpriteRenderer>();
            healthbarRenderer.sortingOrder = 1;
            healthbarRenderer.sprite = healthbar.defaultHealthbarSprite;
        }
        healthbarRenderer.color = healthbar.GetColor(healthPoints, baseHealthPoints);

        // Position and Scale the Healthbar
        // Find reference points to calculate position and scale
        SpriteRenderer[] renderer = gameObject.GetComponentsInChildren<SpriteRenderer>().
            Where(x => x.name != "Healthbar").ToArray();
        Vector3 parentOrigin = gameObject.transform.position;
        Vector3 sizeDelta = Vector3.zero;
        Vector3 start = Vector3.zero;
        int len = renderer.Length;
        for (int i = 0; i < len; i++)
        {
            Color col = renderer[i].color;
            Vector3 size = renderer[i].bounds.size;
            // xMyM points to the middle of the Sprite in world coordinates
            Vector3 xMyM = (renderer[i].gameObject.transform.position);
            Vector3 x0y0 = (-size / 2f);
            Vector3 x1y0 = new Vector3(size.x, -size.y) / 2f;
            Vector3 x1y1 = (-x0y0);
            Vector3 x0y1 = new Vector3(-size.x, size.y) / 2f;

            Vector3 v00 = (xMyM + x0y0) - parentOrigin;
            Vector3 v10 = (xMyM + x1y0) - parentOrigin;
            Vector3 v11 = (xMyM + x1y1) - parentOrigin;
            Vector3 v01 = (xMyM + x0y1) - parentOrigin;
            if (debug)
            {
                Debug.LogFormat("v00 => {0}\tv01 => {1}\nv10 => {2}\tv11{3}", v00, v01, v10, v11);
                Debug.LogFormat("Min({0}, {1}, {2}\n({3}, {4}, {5})", start.x, v00.x, v01.x, start.y, v00.y, v10.y);
                Debug.LogFormat("Max({0}, {1}, {2}\n({3}, {4}, {5}", sizeDelta.x, v11.x, v10.x, sizeDelta.y, v11.y, v01.y);
            }
            start.x = Mathf.Min(start.x, v00.x, v01.x);
            start.y = Mathf.Min(start.y, v00.y, v10.y);
            sizeDelta.x = Mathf.Max(sizeDelta.x, v11.x, v10.x);
            sizeDelta.y = Mathf.Max(sizeDelta.y, v11.y, v01.y);
            if (debug)
            {
                Debug.LogFormat("start => {0}\nsizeDelta => {1}", start, sizeDelta);
                Debug.DrawRay(parentOrigin, v00, Color.blue);
                Debug.DrawRay(parentOrigin, v10, Color.green);
                Debug.DrawRay(parentOrigin, v11, Color.red);
                Debug.DrawRay(parentOrigin, v01, Color.green);

                Debug.DrawRay(xMyM, x0y0, col);
                Debug.DrawRay(xMyM, x1y0, col);
                Debug.DrawRay(xMyM, x1y1, col);
                Debug.DrawRay(xMyM, x0y1, col);

                Debug.DrawRay(parentOrigin + start, (xMyM + x0y0) - (parentOrigin + start), col.Ext_Invert());
                Debug.DrawRay(parentOrigin + start, (xMyM + x1y0) - (parentOrigin + start), col.Ext_Invert());
                Debug.DrawRay(parentOrigin + start, (xMyM + x1y1) - (parentOrigin + start), col.Ext_Invert());
                Debug.DrawRay(parentOrigin + start, (xMyM + x0y1) - (parentOrigin + start), col.Ext_Invert());
            }
        }
        Vector3 scale = gameObject.transform.localScale;
        Vector3 healthbarDim = new Vector3((sizeDelta.x - start.x) * (1f / scale.x), healthbar.width * (1f / scale.y), 1f);
        Vector3 healthbarPos = new Vector3(0f, (1f / scale.y) * (sizeDelta.y + healthbar.distance), 0f);

        if (debug) Debug.DrawRay(parentOrigin, healthbarPos, Color.red);
        if (debug) Debug.DrawRay(parentOrigin + healthbarPos - healthbarDim / 2f, healthbarDim, Color.green);

        healthbarGO.transform.localPosition = healthbarPos;
        healthbarGO.transform.localScale = healthbarDim;
        // SetUpIndicator(Vector3.zero, healthbarDim);
    }
}