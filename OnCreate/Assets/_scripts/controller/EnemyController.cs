﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : PlayerController {
    private AIResourceController aiUnitResourceController;
    void Start()
    {
        float x = Camera.main.ScreenToWorldPoint(InterfaceManager.Instance.ScreenSize * 0.9f).x;
        allyBase.transform.position = new Vector3(x, -0.6f, 0f);
        aiUnitResourceController = GetComponent<AIResourceController>();
        units = new Pool<UnitController>[unitLineup.Length];
        for (int i = 0; i < units.Length; i++)
        {
            int t = i;
            units[i] = new Pool<UnitController>(() => {
                return PoolFactory(t);
            });
        }
        StartCoroutine(Behaviour());
    }
    public float spawnCooldown;

    private IEnumerator Behaviour()
    {
        HealthbarController myHealth = allyBase.GetComponent<HealthbarController>();
        HealthbarController enemyHealth = enemyBase.GetComponent<HealthbarController>();
        float timePassedSinceSpawn = 0f;
        while (myHealth.IsAlive && enemyHealth.IsAlive)
        {
            timePassedSinceSpawn += Time.deltaTime;
            if (timePassedSinceSpawn > spawnCooldown)
            {
                int r = Mathf.FloorToInt(Random.Range(0, unitLineup.Length));
                if (aiUnitResourceController.CanPay(unitLineup[r], aiUnitResourceController.currentResourceAmount))
                {
                    this.Spawn(r);
                }
                timePassedSinceSpawn = 0f;
            }
            yield return null;
        }
    }

    private UnitController PoolFactory(int index)
    {
        Unit u = unitLineup[index];
        GameObject go = new GameObject(name + "|" + u.name);
        go.transform.SetParent(transform);
        go.layer = gameObject.layer;
        UnitController uc = go.Ext_AddOrGetCompononent<UnitController>();
        uc.unit = u;
        uc.enemyMask = enemyMask;
        uc.target = enemyBase.transform;
        uc.Ready(this, index);
        return uc;
    }
    public override void Spawn(int index)
    {
        UnitController controller = null;
        if (aiUnitResourceController.PayCostAI(unitLineup[index]))
        {
            controller = units[index].Acquire();
            controller.Ready(this, index);
            if (debug) Debug.Log("Acquiring Unit " + controller.unit.name + " with index " + index);
        }
    }
}
