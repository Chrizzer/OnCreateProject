﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Resource")]
public class Resource : ScriptableObject {

    public enum Type
    {
        Unit, Spell
    }
    public new string name;
    public int baseMinimum;
    public int baseMaximum;
    public int regenerationPerSecond;
    public Type type;
}
