﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceController : MonoBehaviour {
    public bool debug;
    public Resource resource;
    public int currentResourceAmount;
    public int currentRegenerationAmount;
    public int currentMaximum;
    public float currentRegenerationRate;

    void Start()
    {
        this.currentResourceAmount = resource.baseMinimum;
        this.currentMaximum = resource.baseMaximum;
        this.currentRegenerationRate = 1f / resource.regenerationPerSecond;
        if (resource.type == Resource.Type.Unit)
        {
            InterfaceManager.Instance.BuildResourceGUI(resource);
        }
        else
        {
            InterfaceManager.Instance.BuildManaGUI(resource);
        }
        StartCoroutine(RegenerationCycle());
    }

    public virtual IEnumerator RegenerationCycle()
    {
        while (gameObject.activeInHierarchy)
        {
            IncreaseResource(currentRegenerationAmount);
            yield return new WaitForSeconds(currentRegenerationRate);
        }
    }

    public virtual void IncreaseResource(int amount)
    {
        if ((currentResourceAmount + amount) < currentMaximum)
        {
            currentResourceAmount += amount;
        }
        else
        {
            currentResourceAmount = currentMaximum;
            if (debug) Debug.LogWarning("Resource "+resource.name + " is full");
        }
        UpdateGUI();
    }
    private void UpdateGUI()
    {
        if (resource.type == Resource.Type.Unit)
        {
            InterfaceManager.Instance.UpdateResourceGUI(currentResourceAmount);
        }
        else
        {
            InterfaceManager.Instance.UpdateManaGUI(currentResourceAmount);
        }
    }
    public virtual bool PayCost(Card card)
    {
        if (card.baseCost < currentResourceAmount)
        {
            currentResourceAmount -= card.baseCost;
            UpdateGUI();
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool PayCostAI(Card card)
    {
        if (card.baseCost < currentResourceAmount)
        {
            currentResourceAmount -= card.baseCost;
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool CanPay(Card card, int resourceAmount)
    {
        return card.baseCost < resourceAmount;
    }
}
