﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollAble : MonoBehaviour//, IDragHandler
{
    public void OnDrag(PointerEventData eventData)
    {
        Vector3 dragV = new Vector3(0f, eventData.delta.y, 0f);
        GetComponent<RectTransform>().position += dragV;
    }
}
