﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragAble : MonoBehaviour, IDragHandler, IDropHandler
{
    public void OnDrag(PointerEventData eventData)
    {
        Vector3 dragV = new Vector3(0f, eventData.delta.y, 0f);
        GetComponent<RectTransform>().position += dragV;
    }

    public void OnDrop(PointerEventData eventData)
    {

    }
}
