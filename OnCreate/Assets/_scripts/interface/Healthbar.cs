﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Healthbar")]
public class Healthbar : ScriptableObject {

    public float width;
    public float distance;
    public ColorInfo[] breakpoints;
    public Sprite defaultHealthbarSprite;
    /// <summary>
    /// Scales the value according to the maximumHealthPoints. 
    /// E.g. value = 75, maximumHealthPoints = 1000 => return 750
    /// </summary>
    /// <param name="value"></param>
    /// <param name="maximumHealthPoints"></param>
    /// <returns></returns>
    public static int ScaleWithMaxHP(int value, int maximumHealthPoints)
    {
        return value * maximumHealthPoints / 100;
    }
    public Color GetColor(int healthpoints, int maximumHealthPoints)
    {
        return GetTheColor(healthpoints, maximumHealthPoints);
    }
    private Color GetTheColor(int healthpoints, int maximumHealthPoints = 0, int index = 0)
    {
        int scaledPoints = ScaleWithMaxHP(breakpoints[index].value, maximumHealthPoints);
        if (scaledPoints < healthpoints && index+1 < breakpoints.Length)
        {
            return GetTheColor(healthpoints, maximumHealthPoints, index + 1);
        }
        else
        {
            return breakpoints[index].color;
        }
    }
}