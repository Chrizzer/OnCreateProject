﻿using UnityEngine;

[System.Serializable]
public class ColorInfo
{
    public string name;
    public int value;
    public Color color;
    public ColorInfo(int v, Color c)
    {
        this.value = v;
        this.color = c;
    }
}