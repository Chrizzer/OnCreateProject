﻿using UnityEngine;

/// <summary>
/// Base class for every Singleton class.
/// <para>Useage is MySingleton : Singleton&lt;MySingleton&gt;</para>
/// </summary>
/// <typeparam name="T">Name of the class</typeparam>
public class Singleton<T> : MonoBehaviour where T : Component
{
    private static T _Instance = null;
    public static T Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType(typeof(T)) as T;
            }

            if (_Instance == null && Application.isPlaying)
            {
                GameObject obj = GameObject.FindGameObjectWithTag("Singleton");
                if (obj == null)
                {
                    obj = new GameObject("SingletonGO");
                    obj.tag = "Singleton";
                }
                _Instance = obj.AddComponent(typeof(T)) as T;
                Debug.LogWarning("Singleton had to be instantiated.");
            }
            return _Instance;
        }
    }
    void OnEnable()
    {
        //DontDestroyOnLoad(gameObject);
    }
}