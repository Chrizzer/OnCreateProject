﻿[System.Serializable]
public class UnitProperty<T>
{
    private T initialValue;
    public T value;
    public UnitPropertyType type;
    public bool mutable;

    public UnitProperty(T value, UnitPropertyType type)
    {
        this.initialValue = value;
        this.value = value;
        this.type = type;
        this.mutable = true;
    }
    public UnitProperty(T value, UnitPropertyType type, bool mutable)
    {
        this.initialValue = value;
        this.value = value;
        this.type = type;
        this.mutable = mutable;
    }
    public void Reset()
    {
        if (mutable)
        {
            this.value = initialValue;
        }
    }
}
